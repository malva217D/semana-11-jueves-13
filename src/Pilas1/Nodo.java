package Pilas1;

public class Nodo {

    String dato;
    Nodo siguiente;

    public Nodo(String valor) {
        this.dato = valor;
        this.siguiente = null;
    }

}
