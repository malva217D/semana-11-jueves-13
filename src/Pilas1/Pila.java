package Pilas1;

import javax.swing.JOptionPane;

/**
 *
 * @author M@rlon Alvarado
 */
public class Pila {

    private Nodo ultimo;
    int tamano;
    String lista;

    // Constructor de la Pila
    public Pila() {
        ultimo = null;
        tamano = 0;
    }

    //Devuelve si la pila esta vacia
    public boolean pilaVacia() {
        return ultimo == null;
    }

    //Metodo insertado un Nodo en la Pila
    public void insertarNodo(String pnodo) {
        Nodo nuevo = new Nodo(pnodo);
        nuevo.siguiente = ultimo;
        ultimo = nuevo;
        tamano++;
    }

    // Metodo Eliminar un Nodo de la Pila
    public String eliminarNodo() {
        String temporal = ultimo.dato;
        ultimo = ultimo.siguiente;
        tamano--;
        return temporal;
    }

    //Metodo ver cual fue el ultimo Nodo ingresado
    public String ultimoNodoIngresado() {
        return ultimo.dato;
    }

    //Metodo ver el tamaño de la Pila
    public int verTamano() {
        return tamano;
    }

    //Metodo para Limpiar la Pila
    public void vaciarPila() {
        while (!pilaVacia()) {
            eliminarNodo();
        }
    }

    //Metodo mostrar todo los registros de la Pila
    public void mostrarPila() {
        Nodo recorre = ultimo;
        while (recorre != null) {
            lista += recorre.dato + "\n";
            recorre = recorre.siguiente;
        }
        JOptionPane.showMessageDialog(null, lista);
        lista = "";
    }

}
