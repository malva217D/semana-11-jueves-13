package Pilas1;

import java.util.ArrayList;
import java.util.Stack;

public class Home {
    
    public static void main(String[] args) {
        
        Stack pila = new Stack();
        ArrayList<String> provincias = new ArrayList<String>();
        
        provincias.add("Alajuela");
        provincias.add("Heredia");
        provincias.add("Cartago");
        provincias.add("Limon");
        provincias.add("San Carlos");
        
        Persona persona = new Persona("217", "Marlon");

        //PUSH-- Ingresa un Elemento a la Pila
        //POP-- Sacar el elemento de la Pila
        //PEEK-- ver cual es el Ultimo elemento de la Pila
        //EMPTY-- validar si la pila esta vacia
        //SEARCH-- ver que tan abajo esta el Nodo que busco del top de la pila
        //LIFO
        pila.push(1);
        pila.push("Cadena Texto");
        pila.push(6);
        pila.push("Hola Mundo");
        pila.push(provincias);
        pila.push(persona);

        //System.out.println(pila.peek());
        //System.out.println(pila.pop());
        //System.out.println(pila.peek());
        //System.out.println(pila.empty());
        System.out.println(pila.search(persona));
        
    }
    
}
