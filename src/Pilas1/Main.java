package Pilas1;

import javax.swing.JOptionPane;

public class Main {

    public static void main(String[] args) {

        int opcion = 0;
        String nodo;
        Pila pila = new Pila();

        do {
            opcion = Integer.parseInt(
                    JOptionPane.showInputDialog(
                            "Menu de Opciones \n\n"
                            + "1. Insertar Elemento a la Pila\n"
                            + "2. Eliminar Elemento de la Pila\n"
                            + "3. Imprimir toda la Pila\n"
                            + "4. Ver Ultimo Elemento de la Pila\n"
                            + "5. Vaciar la Pila\n"
                            + "6. La Pila esta Vacia?\n"
                            + "7. Ver el Tamano de la Pila\n"
                            + "8. Salir "));
            switch (opcion) {
                case 1:
                    nodo = JOptionPane.showInputDialog(null, "Ingrese el Nombre: ");
                    pila.insertarNodo(nodo);
                    break;
                case 2:
                    if (!pila.pilaVacia()) {
                        JOptionPane.showMessageDialog(null, "Se a Eliminado el Nodo "
                                + pila.eliminarNodo());

                    } else {
                        JOptionPane.showMessageDialog(null, "La Pila Esta Vacia");
                    }
                    break;
                case 3:
                    if (!pila.pilaVacia()) {
                        pila.mostrarPila();

                    } else {
                        JOptionPane.showMessageDialog(null, "La Pila esta Vacia ");
                    }
                    break;
                case 4:
                    if (!pila.pilaVacia()) {
                        JOptionPane.showMessageDialog(null, "El Tope de la Pila es "
                                + pila.ultimoNodoIngresado());

                    } else {
                        JOptionPane.showMessageDialog(null, "La Pila ya esta Vacia ");
                    }
                    break;
                case 5:
                    if (!pila.pilaVacia()) {
                        pila.vaciarPila();

                    } else {
                        JOptionPane.showMessageDialog(null, "La Pila ya esta Vacia ");
                    }
                    break;
                case 6:
                    if (!pila.pilaVacia()) {
                        JOptionPane.showMessageDialog(null, "La Pila NO esta Vacia ");

                    } else {
                        JOptionPane.showMessageDialog(null, "La Pila SI esta Vacia ");
                    }
                    break;
                case 7:
                    JOptionPane.showMessageDialog(null, "La Pila tiene " + pila.verTamano() + " Elementos ");
                    break;
                case 8:
                    System.exit(0);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opcion Incorrecta ");
                    break;
            }
        } while (opcion != 0);

    }

}
